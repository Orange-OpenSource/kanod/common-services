#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import ipaddress
import subprocess

from kanod_configure import common


def configure_dhcp(arg: common.RunnableParams):
    dhcp = arg.conf.get('dhcp_service', None)
    if dhcp is None:
        return
    networks = dhcp.get('networks', None)
    vars = []
    for network in networks:
        net = network.get('cidr', None)
        start = network.get('start', None)
        end = network.get('end', None)
        if net is None or start is None or end is None:
            continue
        print(f'* {net}')
        net_desc: ipaddress.IPv4Network = ipaddress.ip_network(net)
        prefix = net_desc.network_address.compressed
        mask = net_desc.netmask.compressed
        broadcast = net_desc.broadcast_address.compressed
        description = {
            'prefix': prefix,
            'mask': mask,
            'start': start,
            'end': end,
            'broadcast': broadcast
        }
        router = network.get('router', None)
        if router is not None:
            description['router'] = router
        dns = network.get('dns', None)
        if dns is not None:
            description['dns'] = dns
        vars.append(description)
    common.render_template(
        'dhcp.tmpl',
        'etc/dhcp/dhcpd.conf',
        {'networks': vars},
    )
    if dhcp.get('active', True):
        subprocess.run(['systemctl', 'enable', 'dhcpd'])
        subprocess.run(['systemctl', 'start', 'dhcpd'])


common.register('DHCP service configuration', 200, configure_dhcp)
