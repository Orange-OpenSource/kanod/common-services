## template:jinja
server {
    listen      80;
    server_name {{ip_address}};
    location / {
      proxy_pass http://127.0.0.1:8081/;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
}

server {
    # Setup HTTPS certificates
    listen       443 default ssl;
    server_name  {{ip_address}};
    ssl_certificate      /etc/nginx/certs/cert.pem;
    ssl_certificate_key  /etc/nginx/certs/key.pem;
    # Proxy for Maven registry
    location / {
      proxy_pass    http://127.0.0.1:8081/;
      proxy_set_header Host $host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto "https";
    }
}

server {
    # Setup HTTPS certificates
    listen       8443 default ssl;
    server_name  {{ip_address}};
    ssl_certificate      /etc/nginx/certs/cert.pem;
    ssl_certificate_key  /etc/nginx/certs/key.pem;
    # Proxy for Docker registry
    location / {
      proxy_pass    http://127.0.0.1:8082/;
      proxy_set_header Host $host:8443;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Proto "https";
    }
}
