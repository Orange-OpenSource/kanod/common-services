#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import os
from pathlib import Path
import subprocess
import sys
import uuid

from kanod_configure import common

from . import kanod_docker


def start_docker(arg: common.RunnableParams):
    subprocess.run(['systemctl', 'enable', '--now', 'docker'])


def configure_firewalld(arg: common.RunnableParams):
    subprocess.run(['systemctl', 'enable', '--now', 'firewalld'])
    subprocess.run(['/usr/local/bin/configure-firewalld'])


def configure_nexus(arg: common.RunnableParams):

    nexus = arg.conf.get('nexus_service', None)
    if nexus is None:
        return

    def get_passwd(name):
        passwd = nexus.get(name, None)
        if passwd is None:
            passwd = str(uuid.uuid4())
            nexus[name] = passwd
        return passwd

    key = nexus.get('key', None)
    cert = nexus.get('certificate', None)
    if key is not None and cert is not None:
        folder = Path('/etc/nginx/certs')
        os.mkdir(folder)
        with open(folder.joinpath('key.pem'), 'w') as fd:
            fd.write(key)
            fd.write('\n')
        with open(folder.joinpath("cert.pem"), 'w') as fd:
            fd.write(cert)
            fd.write('\n')
    ip_address = nexus.get('ip', None)
    if ip_address is None:
        raise Exception('Cannot configure nexus without endpoint.')
    common.render_template(
        'nexus_proxy.tmpl',
        'etc/nginx/sites-available/proxy',
        {'ip_address': ip_address})
    kanod_docker.container_engine_docker_config(arg.conf)
    subprocess.run(['/usr/local/bin/launch-nexus', ip_address])
    admin_password = get_passwd('admin')
    docker_password = get_passwd('docker')
    maven_password = get_passwd('maven')
    command = [
        '/usr/local/bin/configure-nexus',
        admin_password,
        maven_password,
        docker_password]
    proc = subprocess.run(
        command, stdout=sys.stdout, stderr=subprocess.STDOUT)
    if proc.returncode != 0:
        sys.exit('Configuring nexus failed.')
    common.render_template(
        'nexus_password.tmpl',
        '/home/admin/nexus.yaml',
        {'admin': admin_password,
         'maven': maven_password,
         'docker': docker_password},
    )


common.register('Configure firewalld', 160, start_docker)
common.register('Configure firewalld', 170, configure_firewalld)
common.register('Configure Nexus', 200, configure_nexus)
