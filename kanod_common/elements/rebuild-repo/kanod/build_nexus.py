#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import os
from os import path
from pathlib import Path
import shutil
import stat
import subprocess
import sys
import tempfile
from urllib import parse as urlparse
from urllib import request
import yaml

from kanod_configure import common


def build_nexus(arg: common.RunnableParams):
    nexus = arg.conf.get('nexus_service', None)
    if nexus is None:
        return
    admin_password = nexus.get('admin')
    docker_password = nexus.get('docker')
    build_conf = nexus.get('build', None)
    if build_conf is None:
        return
    git = build_conf.get('git', None)
    if git is None:
        sys.exit('Git URL must be provided')
    git_proxy = build_conf.get('git_proxy', None)
    if git_proxy is not None:
        command = ['git', 'config', '--global', 'http.proxy', git_proxy]
        subprocess.run(command)
    git_key = build_conf.get('git_key', None)
    file_key = '/root/git_key'
    if git_key is not None:
        with open(file_key, 'w') as fd:
            fd.write(git_key)
            fd.write('\n')
        os.chmod(file_key, stat.S_IRUSR | stat.S_IWUSR)
        command = [
            'git', 'config', '--global', 'core.sshCommand',
            'ssh -oStrictHostKeyChecking=no -i /root/git_key'
        ]
        subprocess.run(command)

    env = os.environ.copy()
    env['GIT_URL'] = git
    env['NEXUS_ADMIN_PASSWORD'] = admin_password
    env['NEXUS_KANOD_PASSWORD'] = docker_password
    proxy = env.get('http_proxy', None)
    if proxy is not None:
        env['HTTP_PROXY'] = proxy
        proxy_https = env.get('https_proxy', None)
        if proxy_https is not None:
            env['HTTPS_PROXY'] = proxy_https
        env['RUNNER_PROXY_ACTIVE'] = 'true'
        parsed = urlparse.urlparse(proxy)
        env['RUNNER_PROXY_PORT'] = (
            str(parsed.port) if parsed.port is not None
            else '443' if parsed.scheme == 'https' else '80')
        env['RUNNER_PROXY_HOST'] = (
            str(parsed.hostname) if parsed.hostname is not None
            else 'localhost')
    else:
        # Dummy config to avoid complaints from maven
        env['RUNNER_PROXY_ACTIVE'] = 'false'
        env['RUNNER_PROXY_PORT'] = '0'
        env['RUNNER_PROXY_HOST'] = 'localhost'
    file = '/etc/kanod-configure/jobs.yaml'
    if 'manifest' in build_conf:
        manifest = build_conf['manifest']
        with open(file, 'w') as fd:
            yaml.dump(manifest, fd)
    elif 'url' in build_conf:
        url = build_conf['url']
        request.urlretrieve(url, file)
    elif 'project' in build_conf:
        project = build_conf['project']
        project_path = project.get(
            'path', 'jobs-unified-build.git')
        file_path = project.get('file', 'jobs.yaml')
        url = path.join(git, project_path)
        rev = project.get('revision', 'master')
        project_dir = Path('/opt/jobs-unified-build')
        command = [
            'git', 'clone', '--depth=1', '--branch', rev, url, project_dir
        ]
        if subprocess.run(command).returncode != 0:
            raise Exception(f'Cannot fetch the unified project: {url}')
        job_file = project_dir.joinpath(file_path)
        if not path.exists(job_file):
            raise Exception('Did not find the job file in the unified project')
        shutil.copy(job_file, file)
    else:
        raise Exception('Unknown method for rebuild')
    tempdir = tempfile.mkdtemp()
    command = [
        'kanod-trigger-build', '--pipelines', file, '--build-dir', tempdir]
    proc = subprocess.run(
        command, env=env, stdout=sys.stdout, stderr=subprocess.STDOUT)
    shutil.rmtree(tempdir)
    if proc.returncode != 0:
        raise Exception('Nexus provisioning failed')
    command = ['docker', 'system', 'prune', '-f', '-a']
    subprocess.run(command)


common.register('Rebuild nexus repository', 200, build_nexus)
