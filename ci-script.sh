#!/bin/bash

#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

basedir=$(dirname "${BASH_SOURCE[0]}")

url=${REPO_URL}
if [ -z "${url}" ]; then
  echo "No Nexus repository defined (fail)"
  exit 1
fi

groupId="kanod"
groupPath=$(echo "$groupId" | tr '.' '/')

version="$COMMON_VERSION"
if [ -n "$version" ]; then
  artifactId="common-services"
  if ! curl --head --silent --output /dev/null --fail "${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.qcow2"; then
    echo "Building common-services image"
    "${basedir}/make-image.sh"
    if [ -f common.qcow2 ]; then
      # shellcheck disable=SC2086
      mvn ${MAVEN_CLI_OPTS} -B deploy:deploy-file  ${MAVEN_OPTS} \
        -DgroupId="${groupId}" -DartifactId="${artifactId}" \
        -Dversion="${version}" -Dtype=qcow2 -Dfile=common.qcow2 \
        -DrepositoryId=kanod -Durl="${url}" \
        -Dfiles="${basedir}/common-schema.yaml" -Dtypes=yaml  -Dclassifiers=schema
    else
        echo "Build for common-services failed"
        exit 1
    fi
  fi
fi

