#!/bin/bash

set -eu
KANODCONFIG="${KANODCONFIG:-${HOME}/.kanod}"
action=backup

while getopts "b:c:hr?" opt; do
    case "$opt" in
    h|\?)
        echo "Usage: backup-nexus.sh [options]"
        echo ""
        echo "Options:"
        echo "    -c alternative configuration folder if used"
        echo "    -b name of backup file"
        echo "    -r restore action (default is backup)"
        exit 0
        ;;
    c)
        KANODCONFIG="${OPTARG}"
        ;;
    r)
        action=restore
        ;;
    b)
        backup="${OPTARG}"
    esac
done

backup="${backup:-${KANODCONFIG}/backup.tgz}"
ip="$(yq e '.nexus_service.ip' "$KANODCONFIG/config.yaml")"

ssh-common() {
    ssh -i "${KANODCONFIG}/key" "admin@${ip}" "$@"
}

case "$action" in
    'backup')
        if ! ssh-common compgen -G "/data/nexus-data/backup/component-*.bak" > /dev/null; then
            echo 'Cannot find backup data. Exiting'
            echo 'You must create a manual task for saving orientdb database to /nexus-data/backup '
            exit 1
        fi
        ssh-common sudo docker stop --time=120 nexus3
        ssh-common tar -C /data/nexus-data -czvf - backup blobs > "$backup"
        ssh-common sudo rm -rf /data/nexus-data/backup
        ssh-common sudo docker restart nexus3
        ;;
    'restore')
        ssh-common sudo docker stop --time=120 nexus3
        ssh-common sudo rm -rf \
            /data/nexus-data/blobs /data/nexus-data/db/config \
            /data/nexus-data/db/component /data/nexus-data/db/security \
            '/data/nexus-data/restore-from-backup/*.bak'
        ssh-common sudo tar -C /data -xzvf - blobs backup < "$backup"
        ssh-common sudo mv /data/blobs /data/nexus-data/blobs
        ssh-common sudo mv '/data/backup/*.bak' /data/nexus-data/restore-from-backup
        ssh-common sudo rm -rf /data/backup
        ssh-common sudo docker restart nexus3
        ;;
    *)
        echo 'unknown action'
        ;;
esac
