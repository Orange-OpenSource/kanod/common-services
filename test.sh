#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


BASEDIR=$(dirname "${BASH_SOURCE[0]}")

target=${1:-common.qcow2}
tmpdir=$(mktemp -d)
if virsh list --name --state-running | grep -q '^test$' ; then
  virsh destroy test
fi
if virsh list --name --all | grep -q '^test$' ; then
  virsh undefine test
fi

rm -f test.qcow2
rm -f cidata.iso
cp "$target" test.qcow2
envsubst < "${BASEDIR}/user-data" > "${tmpdir}/user-data"

cat > "${tmpdir}/meta-data" <<EOF
instance-id: ${VM_NAME}
local-hostname: ${VM_NAME}
EOF


genisoimage -output cidata.iso -volid cidata -joliet -rock -graft-points ".=$tmpdir"
virt-install --noautoconsole --import --name test --ram 4096 --vcpus 2 \
  --cpu host-passthrough --os-type linux \
   --disk "path=test.qcow2,bus=virtio,format=qcow2" \
   --disk "cidata.iso,device=cdrom" --network network=default,model=virtio

rm -rf "$tmpdir"
