Overview
========

Common services is a machine that hosts:

* A Nexus artifact manager with dual views as a Maven repository and as 
  a Docker container registry. The service is accessible though http on
  ports 80 (maven) and 8082 (docker) and through https on ports 443 and 8443
* An optional NTP server that can be used as base by other machines
* An optional DHCP server. This DHCP server **does not replace** the 
  Ironic DHCP server used by Metal3 to bootstrap baremetal hosts.

When the Nexus server is launched it is empty. Here are some tips and tricks to populate it:

* The build is triggered by the ``.nexus_service.build`` element.
* You can use internal docker registry mirrors to avoid DockerHub
  limits.
* Next you can build your Nexus on one site and use it elsewhere. You
  need to connect as admin user on the common service machine with ssh.
  Launch ``nexus-backup`` and export the obtained backup archive.
  On the other site, do not put a build section in the configuration of
  ``common-services``. Transfer your archive and use ``nexus-restore``.
