# common-serices

Builds an image for a server hosting all the common services needed by Kanod.
It is fully configured by kanod-configure launched during the cloud-init phase.

This project should provide:
* an image `common-services-xxx.qcow2`
* a schema

They can be used with `kanod-prepare` and `kanod-vm` with a suitable 
`config.yaml` configuration:

Run `./make-image.sh` to build the image
The following variables may provide some configuration options:
* `REGISTRY_MIRROR` a docker mirror to use. Used by skopeo to avoid dockerhub
  rate limiting
* `DIB_REPOREF_kanodconfigure` version of kanod-configure to use
* `DIB_REPOREF_kanodbuild` version of kanod-unified-build to use
* `DEBUG` builds a debug version with user `dev-user` default password `secret`
  (to debug networking problems only).

  # Backups/Restore

  There are two ways to make a backup
  * A deprecated internal backup that can be run as admin inside the VM
  * An external backup script that is favoured because of the size of the backup.

  In all cases it is necessary to perform a backup task in the Nexus while logged
  as admin. This backup task must be defined manually:
  * In Administration/System/Tasks, create a new task called `backup` of
  type `Admin - Export databases for backup Task`  with target folder:
  `/nexus-data/backup`. The task should be manual.
  * Run the task manually and launch one of the above backup procedure.
